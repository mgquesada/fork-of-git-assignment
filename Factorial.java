//Find Factorial of given integer
import java.util.Scanner;
//Factorial Class
class Factorial
{
   public static void main(String args[])
   {
      int n, c, fact = 1;
 
      System.out.println("Enter an integer to calculate it's factorial");
      //Comment to conflict taking inputs
      Scanner in = new Scanner(System.in);
 
      n = in.nextInt();
 //base case
      if ( n < 0 )
         System.out.println("Number should be non-negative.");
      else
      {
//calculate the factorial

         for ( c = 1 ; c <= n ; c++ )
            fact = fact*c;
 
         System.out.println("Factorial of "+n+" is = "+fact);
      }
   }
}
